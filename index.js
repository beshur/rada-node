var http = require('http');
var express = require('express');
var WebSocketServer = require('ws').Server
var htmlfile = "index.html";
var fs = require('fs');
var app = express();
var jade = require('jade');
var httpServer = http.createServer(app);
var wss = new WebSocketServer({ port: 3000 });

app.render = function (tpl, data) {
	var html = fs.readFileSync('templates/' + tpl + '.jade.html').toString(),
		fn = jade.compile(html);

	return fn(data);
};

app
	.get('/', function(request, response) {
		var html = fs.readFileSync(htmlfile).toString();
		response.send(html);
	});

wss
	.on('connection', function connection(ws) {
	  	var html = fs.readFileSync(htmlfile).toString();
		ws.send('something');

		ws.on('message', function (data) {
			var response = {};

			switch (data.action) {
				case "deputy_form":
					response.html = app.render('deputy_form', {first_name: 'Oleg', last_name: 'Lyashko'});
					response.action = 'render';
					break;

			}

			if (response.action)
				ws.send(response);
		})
	});

var port = process.env.PORT || 8090;
httpServer.listen(port, function() {
	console.log("Listening on " + port);
});